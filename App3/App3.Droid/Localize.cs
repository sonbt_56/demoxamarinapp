

using System;
using Xamarin.Forms;
using System.Threading;
using System.Globalization;

[assembly:Dependency(typeof(App3.Droid.Localize))]

namespace App3.Droid {
    public class Localize : App3.ILocalize {
        public System.Globalization.CultureInfo GetCurrentCultureInfo() {
            var androidLocale = Java.Util.Locale.Default;

            //var netLanguage = androidLocale.Language.Replace ("_", "-");
            var netLanguage = androidLocale.ToString().Replace("_", "-");

            //var netLanguage = androidLanguage.Replace ("_", "-");
            System.Diagnostics.Debug.WriteLine("android:" + androidLocale.ToString());
            System.Diagnostics.Debug.WriteLine("net:" + netLanguage);

            System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.CurrentCulture);
            System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.CurrentUICulture);

            return new System.Globalization.CultureInfo(netLanguage);
        }

        public void SetLocale() {
            var androidLocale = Java.Util.Locale.Default; // user's preferred locale
            var netLocale = androidLocale.ToString().Replace("_", "-");
            var ci = new System.Globalization.CultureInfo(netLocale);

            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
        }

        public void SetLocale(CultureInfo cultureInfo) {

            System.Diagnostics.Debug.WriteLine("language set to "+cultureInfo.ToString());
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
        }
    }
}
