﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

using Xamarin.Forms;

namespace App3 {
    public partial class App : Application {
        public App() {

            System.Diagnostics.Debug.WriteLine("===============");
            var assembly = typeof(App).GetTypeInfo().Assembly;
            foreach (var res in assembly.GetManifestResourceNames())
                System.Diagnostics.Debug.WriteLine("found resource: " + res);

            if (Application.Current.Properties.ContainsKey("language_code")) {

                var languageCode = Application.Current.Properties["language_code"] as string;
                System.Diagnostics.Debug.WriteLine("found setted language code: " + languageCode);
                var ci = new System.Globalization.CultureInfo(languageCode);

                if (Device.OS != TargetPlatform.WinPhone) {
                    DependencyService.Get<ILocalize>().SetLocale(ci);
                }

            } else {
                if (Device.OS != TargetPlatform.WinPhone) {
                    DependencyService.Get<ILocalize>().SetLocale();
                    //Resx.AppResources.Culture = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
                }
            }



            InitializeComponent();
            MainPage = new XamlPage.MainPage();
        }

        protected override void OnStart() {
            // Handle when your app starts
        }

        protected override void OnSleep() {
            // Handle when your app sleeps
        }

        protected override void OnResume() {
            // Handle when your app resumes
        }
    }
}
