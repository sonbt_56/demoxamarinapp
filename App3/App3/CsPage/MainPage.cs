﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App3.CsPage {

    class MainPage : MasterDetailPage {

        public MainPage() {
            Master = new XamlPage.MasterPage();
            Detail = new NavigationPage(new TransactionPage());
        }
    }
}
