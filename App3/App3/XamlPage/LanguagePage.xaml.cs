﻿using App3.Res;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace App3.XamlPage {
    public partial class LanguagePage : ContentPage {

        string[] languageCodes = {"en","ja","pt","pt-BR","es","zh-CN","zh-TW","vi"};
        public LanguagePage() {
            InitializeComponent();
            Title = AppResources.Language;
            var myPicker = languagePicker;
            languagePicker.Items.Add(AppResources.English);
            languagePicker.Items.Add(AppResources.Japanese);
            languagePicker.Items.Add(AppResources.Portuguese);
            languagePicker.Items.Add(AppResources.Brazilian);
            languagePicker.Items.Add(AppResources.Spanish);
            languagePicker.Items.Add(AppResources.ChineseS);
            languagePicker.Items.Add(AppResources.ChineseT);
            languagePicker.Items.Add(AppResources.Vietnamese);

            var myButton = applyButton;

            myButton.Clicked += async (sender, e) => {
                Application.Current.Properties["language_code"] = languageCodes[myPicker.SelectedIndex];
                Application.Current.SavePropertiesAsync();
                var ci = new System.Globalization.CultureInfo(languageCodes[myPicker.SelectedIndex]);

                if (Device.OS != TargetPlatform.WinPhone) {
                    DependencyService.Get<ILocalize>().SetLocale(ci);
                }


                // reset the application
                Application.Current.MainPage = new XamlPage.MainPage();
                //((MainPage)Application.Current.MainPage).DetailPage = new NavigationPage((Page)Activator.CreateInstance(typeof(LanguagePage)));
                // note
                string titleMesassage = "Your language have been changed";
                string detailMessage = "Your language have been changed to " + myPicker.Items[myPicker.SelectedIndex];
                await DisplayAlert(titleMesassage, detailMessage, AppResources.OKButton);
            };
        }
    }
}
