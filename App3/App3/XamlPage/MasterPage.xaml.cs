﻿using App3.Items;
using App3.Res;
using App3.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace App3.XamlPage {
    public partial class MasterPage : ContentPage {
        public ListView ListView { get { return listView; } }
        public MasterPage() {
            InitializeComponent();
            var masterPageItems = new List<MasterPageItem>();
            masterPageItems.Add(new MasterPageItem {
                Title = AppResources.NewsFeed,
                IconSource = "news_feed.png",
                TargetType = typeof(NewsFeedPage),
                HasNotification = true,
                NotificationNumber = 10,
                NotificationColor = Color.FromHex("2196F3"),
                
            });

            masterPageItems.Add(new MasterPageItem {
                IsSeperator = true
            });

            masterPageItems.Add(new MasterPageItem {
                Title = AppResources.Transaction,
                IconSource = "transactions.png",
                HasNotification = false,
                TargetType = typeof(CsPage.TransactionPage)
            });

            masterPageItems.Add(new MasterPageItem {
                Title = AppResources.Cart,
                IconSource = "cart.png",
                HasNotification = false,
                TargetType = typeof(CartPage)
            });

            masterPageItems.Add(new MasterPageItem {
                Title = AppResources.Wallet,
                IconSource = "wallet.png",
                HasNotification = true,
                NotificationNumber = 2,
                NotificationColor = Color.FromHex("4CAF50"),
                TargetType = typeof(WalletPage)
            });

            masterPageItems.Add(new MasterPageItem {
                Title = AppResources.BankingInfo,
                IconSource = "banking_info.png",
                HasNotification = true,
                NotificationColor = Color.FromHex("4CAF50"),
                NotificationNumber = 5,
                TargetType = typeof(BankingInfoPage)
            });

            masterPageItems.Add(new MasterPageItem {
                IsSeperator = true
            });

            masterPageItems.Add(new MasterPageItem {
                Title = AppResources.Language,
                IconSource = "language.png",
                HasNotification = false,
                TargetType = typeof(LanguagePage)
            });

            masterPageItems.Add(new MasterPageItem {
                Title = AppResources.AboutUs,
                IconSource = "about_us.png",
                HasNotification = false,
                TargetType = typeof(AboutUsPage)
            });

            listView.ItemTemplate = new DataTemplate(typeof(MasterItemCell));
            listView.ItemsSource = masterPageItems;
            listView.SelectedItem = masterPageItems[0];
        }
    }
}
