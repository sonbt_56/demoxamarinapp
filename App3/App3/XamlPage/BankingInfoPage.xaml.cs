﻿using App3.Res;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace App3.XamlPage {
    public partial class BankingInfoPage : ContentPage {
        public BankingInfoPage() {
            InitializeComponent();
            Title = AppResources.BankingInfo;
        }
    }
}
