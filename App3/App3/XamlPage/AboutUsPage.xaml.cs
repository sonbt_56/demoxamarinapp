﻿using App3.Res;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace App3.XamlPage {
    public partial class AboutUsPage : ContentPage {
        public AboutUsPage() {
            InitializeComponent();
            Title = AppResources.AboutUs;
        }
    }
}
