﻿using App3.CsPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App3.XamlPage {
    public partial class MainPage : MasterDetailPage {

        public Page DetailPage { get { return Detail; } set { Detail = value; } }
        public MainPage() {
            InitializeComponent();
            masterPage.ListView.ItemSelected += OnItemSelected;

        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e) {
            var item = e.SelectedItem as Support.MasterPageItem;
            if (item != null) {
                Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                masterPage.ListView.SelectedItem = item;
                IsPresented = false;
            }
        }

    }
}
