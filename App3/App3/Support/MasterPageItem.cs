﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3.Support {
    class MasterPageItem {
        public string Title { get; set; }

        public string IconSource { get; set; }

        public bool HasNotification { get; set; }

        public int NotificationNumber { get; set; }

        public bool IsSeperator { get; set; }

        public Xamarin.Forms.Color NotificationColor { get; set; }

        public String NotificationString {
            get {
                return NotificationNumber + " news";
            }
        }
        public Type TargetType { get; set; }

    }
}
