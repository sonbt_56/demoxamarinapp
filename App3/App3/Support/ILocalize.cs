﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App3 {
    public interface ILocalize {

        CultureInfo GetCurrentCultureInfo();

        void SetLocale();

        void SetLocale(CultureInfo cultureInfo);
    }
}
