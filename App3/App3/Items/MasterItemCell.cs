﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App3.Items {
    class MasterItemCell : ViewCell {

        public MasterItemCell() {
            var itemIcon = new Image {
                WidthRequest = 35,
                Aspect = Aspect.AspectFill,
                HorizontalOptions = LayoutOptions.Start,
                VerticalOptions = LayoutOptions.Center,
            };
            itemIcon.SetBinding(Image.SourceProperty, "IconSource");

            var nameLabel = new Label() {
                FontFamily = "HelveticaNeue-Medium",
                FontSize = 18,
                TextColor = Color.FromHex("444444"),
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center,
            };
            nameLabel.SetBinding(Label.TextProperty, "Title");

            var notificationView = new Label() {
                Text = "2",
                FontFamily = "HelveticaNeue-Small",
                FontSize = 12,
                TextColor = Color.White,
                BackgroundColor = Color.Blue,
                HorizontalTextAlignment = TextAlignment.Center,
                HeightRequest = 20,
                WidthRequest = 55,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Center,
                IsVisible = false
            };
            notificationView.SetBinding(Label.TextProperty, "NotificationString");
            notificationView.SetBinding(Label.BackgroundColorProperty, "NotificationColor");
            notificationView.SetBinding(Label.IsVisibleProperty, "HasNotification");


            var contentLayout = new StackLayout {
                Spacing = 10,
                Padding = new Thickness(10, 5, 20, 5),
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Children = { itemIcon, nameLabel, notificationView }
            };
        
            this.View = contentLayout;
        }

        protected override void OnBindingContextChanged() {
            base.OnBindingContextChanged();
            dynamic c = BindingContext;

            if(c.IsSeperator == true) {

                var seperator = new BoxView {
                    Color = Color.Silver,
                    HeightRequest = 1,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                };

                View = new StackLayout {
                    Spacing = 5,
                    Orientation = StackOrientation.Horizontal,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Padding = new Thickness(0, 5, 0, 5),
                    Children = { seperator }
                };
            }
        }
    }
}
